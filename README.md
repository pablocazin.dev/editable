# PROJET EDITABLE

### LE COMPOSANT EST UNE FONCTION QUI PRENDS EN COMPTE 
### UNE SOURCE VERS UN FICHIER .JSON , ET QUI CREER UN
### TABLEAU AFFICHANT LES VALEURS, PERMETTANT DE LES MODIFIERS
### DE LES TRIER, DE LES FILTRER, ETC...

#### Pour ajouter un fichier .json
#### -> dans le dossier data/
#### -> ajouter le fichier a utiliser
#### -> dans le dossier editable/editable.js
#### -> ajouter le chemin du fichier a utiliser dans la variable "sourceList"
#### -> le chemin du fichier doit etre de la forme "../data/votre-fichier.json"
#### -> le composant affiche un bouton supplémentaire sur la page test/index.html


#### Le composant est une fonction qui prendre plusieurs parametres, son execution change en fonction de ce qu'elle recoit,
#### elle s'adapte donc au filtre, tri, et sauvegarde les informations modifiées automatiquement.