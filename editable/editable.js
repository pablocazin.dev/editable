// reste a faire
  // terminer le systeme d'ajout d'objet/ligne
  // si le tableau est filtré, appliquer le tri par dessus
  // afficher correctement les données en fonction de leur type (img, date)

function newTab(source, searchBool, searchValue, sortBool, sortCol, sortData) {
  fetch(source)
    .then((response) => response.json())
    .then((data) => {
      let body = document.querySelector("body");
      let script = document.querySelector("script");

      // création de la div accueillant le tableau
      let divTab = document.createElement("div");
      divTab.setAttribute("data-source", source);
      divTab.id = "tab"
      if (sortBool) {
        if (sortData) {
          divTab.setAttribute("data-sort", true);
        } else {
          divTab.setAttribute("data-sort", false);
        }
      }
      body.insertBefore(divTab, script);

      if (sortBool) {
        if (sortData) {
          data.sort(function (a, b) {
            return a[sortCol] > b[sortCol]
              ? -1
              : a[sortCol] < b[sortCol]
              ? 1
              : 0;
          });
        } else {
          data.sort(function (a, b) {
            return a[sortCol] < b[sortCol]
              ? -1
              : a[sortCol] > b[sortCol]
              ? 1
              : 0;
          });
        }
      }
      // Retiens le nombre de colonnes afin que le nombre de fraction grid corresponde au nombre de clées
      let keyCount = 0;
      // le nom des colonnes de la source actuelle, permettet de nommer les labels de input pour visants a creer une nouvelle ligne
      let cols = [];
      // Creation d'autant de
      for (let key in data[0]) {
        cols.push(key);
        let div = document.createElement("div");
        let columnName = document.createTextNode(key);
        div.appendChild(columnName);
        divTab.appendChild(div);
        div.classList.add("case", "header");
        keyCount++;
      }

      divTab.style.gridTemplateColumns = `repeat(${keyCount}, 1fr)`;
      divTab.style.border = "1px solid black";

      // si une valeur est recherchée
      if (searchBool) {
        let whiteList = [];
        // si je trouve un mot qui contient la valeur recherchée je stock son id dans une whitelist
        // je refais une boucle en affichant les objets ayants un id contenu dans la whitelist
        for (let i of data) {
          for (let val in i) {
            let value = String(i[val]);
            if (value.includes(searchValue)) {
              if (!whiteList.includes(i.id)) {
                whiteList.push(i.id);
              }
            }
          }
        }
        let caseBinary = 1;
        for (let i of data) {
          if (whiteList.includes(i.id)) {
            for (let val in i) {
              let div = document.createElement("div");
              let value = document.createTextNode(i[val]);
              div.appendChild(value);
              divTab.appendChild(div);
              div.classList.add("case");
              if (caseBinary <= keyCount) {
                div.classList.add("color-case");
                caseBinary++;
              } else if (caseBinary > keyCount && caseBinary < keyCount * 2) {
                caseBinary++;
              } else if (caseBinary === keyCount * 2) {
                caseBinary = 1;
              }
              div.setAttribute("contenteditable", "true")
              div.setAttribute("data-col", `${val}`)
              div.setAttribute("data-id", `${i.id}`)
              div.setAttribute("data-content", `${i[val]}`)
              div.style.padding = "10px"
            }
          }
        }
        // si il n'y a pas de valeur recherchée
      } else {
        let caseBinary = 1;
        for (let i of data) {
          for (let val in i) {
            let div = document.createElement("div");
            let value = document.createTextNode(i[val]);
            div.appendChild(value);
            divTab.appendChild(div);
            div.classList.add("case");
            if (caseBinary <= keyCount) {
              div.classList.add("color-case");
              caseBinary++;
            } else if (caseBinary > keyCount && caseBinary < keyCount * 2) {
              caseBinary++;
            } else if (caseBinary === keyCount * 2) {
              caseBinary = 1;
            }
            div.setAttribute("contenteditable", "true")
            div.setAttribute("data-col", `${val}`)
            div.setAttribute("data-id", `${i.id}`)
            div.setAttribute("data-content", `${i[val]}`)
            div.style.padding = "10px"
          }
        }
      }
      // retient les éléments modifier et prépare la requete
      let allCase = document.getElementsByClassName("case");
      let dataCol, dataId, dataContent;
      for (let i of allCase) {
        i.addEventListener("input", () => {
          let modifiedAttr = i.getAttributeNames();
          let conditions = ["class", "style", "contenteditable"];
          for (let attr of modifiedAttr) {
            if (!conditions.some((e) => attr.includes(e))) {
              // passer la nouvelles valeur du content au dataset
              i.dataset.content = i.innerHTML;
              // passer les valeurs aux variables
              dataCol = i.dataset.col;
              dataId = i.dataset.id;
              dataContent = i.dataset.content;
            }
          }
          let newData = {id: dataId,col: dataCol,content: dataContent,};
          // envoie des données a up.php
          fetch("../test/up.php", {
            method: "POST",
            headers: {
              Accept: "application/json",
              "Content-Type": "application/json",
            },
            body: JSON.stringify(newData),
          })
            .then((response) => {
              console.log(response.json());
            })
            .catch((response) => {
              console.log(response);
            });
        });
      }
      // appeler la fonction newTab avec un tri
      let headerList = document.getElementsByClassName("header");
      for (let col of headerList) {
        col.addEventListener("click", () => {
          //all il faudra verifier que je prends bien la source actuelle meme sur un tableau filtré
          let actualSource = document.getElementById("tab").dataset.source;
          let divTab = document.getElementById("tab");
          if (divTab.dataset.sort === "true") {
            newTab(actualSource, false, undefined, true, col.innerText, false);
          } else {
            newTab(actualSource, false, undefined, true, col.innerText, true);
          }
          divTab.remove();
        });
      }

      if(document.getElementById('new-obj-container')) {document.getElementById('new-obj-container').remove()}
      let newObjContainer = document.createElement("div");
      newObjContainer.id = "new-obj-container";
      for (let i of cols) {
        let newObj = document.createElement("div");
        newObj.classList.add("input-container");
        newObj.id = i;
        let newObjText = document.createElement("p");
        newObjText.innerText = `${i} :`;
        let newObjInput = document.createElement("input");
        newObjInput.type = "text";
        newObjInput.classList.add("newObj");
        newObj.appendChild(newObjText);
        newObj.appendChild(newObjInput);
        newObjContainer.appendChild(newObj);
      }
      let validButton = document.createElement("div");
      validButton.innerText = "valider"
      validButton.id = "valid-button"
      newObjContainer.appendChild(validButton);
      body.insertBefore(newObjContainer, divTab);

      // ajout des nouvelles informations dans le tableau
      document.getElementById('valid-button').addEventListener("click", () => {
        console.log("ajouter ligne")
        // récuperer les valeurs de chacuns des inputs et les ajouter a data pour
        // afficher le tableau avec les nouvelles valeurs
      })
    })
    .catch((err) => {
      console.log(err);
    });
}
//hors fonction
let body = document.querySelector("body");
let script = document.querySelector("script");
// message de selection des sources
let selectSource = document.createElement("p");
selectSource.innerText = "Sélectionnez une source a afficher";
body.insertBefore(selectSource, script);

// Affichage html des boutons permettants de choisir le fichier json utilisé
let sourceList = [
  "../data/person.json",
  "../data/points.json",
  "../data/product.json",
];
let buttonsWrapper = document.createElement("div");
buttonsWrapper.id = "buttons-wrapper"
for (let source of sourceList) {
  let button = document.createElement("div");
  button.setAttribute("data-source", source);
  button.classList.add("sourceButton")
  let text;
  source.includes("../data/") ? (text = source.slice(8)) : (text = source);
  button.innerHTML = `${text}`;
  buttonsWrapper.appendChild(button);
}
body.insertBefore(buttonsWrapper, script);

// bouton ouvrant le pop up d'ajout au tableau
let newObjButton = document.createElement('div')
newObjButton.id = "new-obj-button"
newObjButton.innerText = "Ajouter au tableau"
body.insertBefore(newObjButton, script)
document.getElementById('new-obj-button').addEventListener('click', () => {
  if(document.getElementById('new-obj-container')) {
    document.getElementById('new-obj-container').style.display = "flex"
  }
})

// creation de la barre de recherche
let searchContainer = document.createElement("div");
searchContainer.id = "search-container";
let inputFilter = document.createElement("input");
inputFilter.id = "filter";
let labelFilter = document.createElement("label");
labelFilter.setAttribute("for", "filter");
labelFilter.id = "filter-button";
labelFilter.innerText = "rechercher";
searchContainer.appendChild(inputFilter);
searchContainer.appendChild(labelFilter);
body.insertBefore(searchContainer, script);

// Changement de la source json au clique
let buttonsList = document.getElementsByClassName("sourceButton");
let actualSource;
for (let button of buttonsList) {
  button.addEventListener("click", () => {
    let divTab = document.getElementById("tab");
    if (divTab) {
      divTab.remove();
    }
    newTab(button.dataset.source, false, undefined, false, undefined, false);
    actualSource = button.dataset.source;
  });
}

// obtenir la valeur entrée dans la barre de recherche, et recreer un tableau en fonction
labelFilter.addEventListener("click", () => {
  let divTab = document.getElementById("tab");
  let searchedValue = inputFilter.value;
  if (actualSource) {
    if (document.getElementById("messageFile")) {
      messageFile.remove();
    }
    if (searchedValue) {
      divTab.remove();
      if (document.getElementById("messageValue")) {
        document.getElementById("messageValue").remove();
      }
      newTab(actualSource, true, searchedValue, false, undefined, false);
    } else {
      if (!document.getElementById("messageValue")) {
        let messageValue = document.createElement("p");
        messageValue.id = "messageValue";
        messageValue.innerText = "Aucune valeur recherchée";
        body.insertBefore(messageValue, divTab);
      }
    }
  } else {
    if (!document.getElementById("messageFile")) {
      let messageFile = document.createElement("p");
      messageFile.id = "messageFile";
      messageFile.innerText = "Veuillez selectionner une source";
      body.insertBefore(messageFile, script);
    }
  }
});
